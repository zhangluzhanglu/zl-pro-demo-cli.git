此包的作用为：创建各种应用实例项目

## 安装：

 npm i zl-pro-demo -g

## 其他命令为：
```js
[
    { "命令": "help", "作用": "查看帮助命令" },
    { "命令": "-v", "作用": "查看版本" },
    { "命令": "postcss-in-gulp", "作用": "创建postcss-in-gulp的demo" },
    { "命令": "mobile-adaptation-rem", "作用": "移动端适配rem解决方案demo" },
    { "命令": "mobile-adaptation-vw", "作用": "移动端适配vw解决方案demo" },
    { "命令": "mobile-adaptation-remAndRem", "作用": "移动端适配rem+vm混合使用demo" },
    { "命令": "pc-adaptation-rem", "作用": "pc端适配rem解决方案demo" },
    { "命令": "postcss-in-react-eject", "作用": "postcss在释放配置后的react应用中使用" },
    { "命令": "postcss-in-react-rewired", "作用": "postcss在重写配置后的react应用中使用" },
    { "命令": "postcss-in-vue", "作用": "postcss在vue中的基本使用" },
    { "命令": "react-demo", "作用": "react官方脚手架demo" },
    { "命令": "vue-demo", "作用": "vue官方脚手架demo" },
    { "命令": "项目地址", "作用": "https://gitee.com/zhangluzhanglu/zl-pro-demo-code.git" },
]
```