const puppeteer = require('puppeteer');
(async () => {
    const browser = await puppeteer.launch({ timeout: 60 * 1000 });//等待 Chrome 实例启动的最长时间。默认为30000（30秒）。如果传入 0 的话则不限制时间
    const page = await browser.newPage()
    await page.goto('https://gitee.com/zhangluzhanglu/zl-pro-demo-code/blob/master/README.md', { timeout: 60 * 1000 });
    await page.waitForSelector(".highlight", { timeout: 60 * 1000 }); //等待元素加载完成，否则后面无法获取内容

    var dataObj = await page.evaluate(() => {
        return JSON.parse(`${document.querySelector(".highlight").innerText}`);
    });
    console.log('dataObj:',typeof dataObj,dataObj);
    await browser.close();
})()
