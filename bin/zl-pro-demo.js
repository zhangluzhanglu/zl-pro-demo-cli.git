#!/usr/bin/env node

let fs = require('fs');
const puppeteer = require('puppeteer');
let child_process = require('child_process');
let proName = process.argv[2];//获取用户输入的参数
// console.log(process.argv);
//显示帮助命令
if (proName === "help") {
    console.log("\n开始从远程服务器获取帮助信息，请稍等......\n");
    (async () => {
        const browser = await puppeteer.launch({ timeout: 60 * 1000 });//等待 Chrome 实例启动的最长时间。默认为30000（30秒）。如果传入 0 的话则不限制时间
        const page = await browser.newPage()
        await page.goto('https://gitee.com/zhangluzhanglu/zl-pro-demo-code/blob/master/README.md', { timeout: 60 * 1000 });
        await page.waitForSelector(".highlight", { timeout: 60 * 1000 }); //等待元素加载完成，否则后面无法获取内容

        let helpDes = await page.evaluate(() => {
            return JSON.parse(`${document.querySelector(".highlight").innerText}`);
        });
        //------------------对帮助信息进行格式化显示---------------
        console.log("\n命令信息如下：\n");
        for (let i = 0; i < helpDes.length; i++) {
            let key = helpDes[i].cli;
            for (let j = helpDes[i].cli.length; j <= 30; j++) {
                key += "-";
            }
            if (helpDes[i].cli == "项目地址") {
                console.log(key, helpDes[i].fun + "\n");
            }
            else {
                console.log("   ", key, helpDes[i].fun + "\n");
            }
        }
        await browser.close();
    })()

}
else if (proName === "-v") {
    console.log("当前版本为:", require('../package.json').version);
}
else {
    getCode(proName)
}
//执行具体的拉取代码的函数
function getCode(proName) {
    console.log("项目拉取中，请稍等......");
    const os = require('os');
    let sysType = os.type();
    let cli = "";
    if (sysType === "Windows_NT") {  //windows
        cli = `git init & git remote add origin https://gitee.com/zhangluzhanglu/zl-pro-demo-code.git &  git config core.sparsecheckout true & echo ${proName} >> .git/info/sparse-checkout & git pull origin master`;
    } else { //mac 和 linux
      cli=`
      git init 
      git remote add origin https://gitee.com/zhangluzhanglu/zl-pro-demo-code.git 
      git config core.sparsecheckout true 
      echo ${proName} >> .git/info/sparse-checkout
      git pull origin master`
    }
    //开始克隆远程postcss-in-gulp项目
    child_process.exec('mkdir zl-pro-demo', function () {
        child_process.exec(cli, { cwd: './zl-pro-demo' },
            function (error, des) {
                if (error !== null) {
                    console.log('\n[项目拉取失败],请检查你的命令是否正确\n\n', error);
                } else {
                    console.log("[项目拉取成功]")
                    deleteFolderRecursive("./zl-pro-demo/.git");//拉取成功后，执行删除.git文件夹的函数（递归的方式）
                }
            });
    });
}


/**
 * 作用：删除一个文件夹的函数
 * 原理：先通过递归方式删除此文件夹下的所有文件，最后再删除文件夹本身
 * @path {string} 准备删除的文件夹的路径
 */
function deleteFolderRecursive(path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file) {
            var curPath = path + "/" + file;
            if (fs.statSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};
